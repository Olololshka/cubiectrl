var data
var plot

var options = {
		lines: {
			show: true
		},
		points: {
			show: true
		},
		xaxis: {
			show: false
		},
		yaxis: { 
			ticks: 5,
			show: true
		},
		series: {
			shadowSize: 0	// Drawing is faster without shadows
		},
	};

function update_mesureValues() {	
	$.ajax({
		url: "/data.api",
		dataType: "json",
		method : "POST",
		cache: false,
		data : { "req" : "mesurment" },
		success : function(v_data) {

			// create data set
			if 	(typeof plot === 'undefined') {	 
				data = new Array();
				for (var serie in v_data) {
					data.push({ label : serie, data : [] })
				}
			} 
			
			// push new value
			var i = 0;
			for (var item in v_data) {
				var serie = v_data[item];
				data[i].data.push( serie["Error"] ? [null, null] :
						[ Date.parse(serie["Timestamp"]), serie["Value"] ]
				);
				i++;
			}

			// shrink last value
			if (data[0].data.length > 50) {
				for (i in data) {
					data[i].data.shift()
				}
			}
			
			// plot
			if (typeof plot !== 'undefined') {
				plot.setData(data);
				plot.setupGrid();
				plot.draw();
			} else {
				plot = $.plot("#plot", data, options);
			}
		}
	});
}

jQuery(document).ready(function($){
	setInterval(update_mesureValues, 500)
});
