package cubiectrl

import (
	"net/http"
	"html/template"
	"fmt"
	"path"
	"runtime"
	"encoding/json"
	"log"
)

var currentPath string
var fileserverHandler http.Handler

func FillPagesMap(m *map[string]func(http.ResponseWriter, *http.Request)) {
	(*m)["/"] = indexHandlr
	(*m)["/asserts"] = assertsServer
	(*m)["/data.api"] = varsJsonHandlr
	
	_, filename, _, _ := runtime.Caller(1)
	currentPath = path.Dir(filename)
	
	fileserverHandler = http.FileServer(http.Dir(currentPath))
}

func patchPath(name1 string, names ...string) []string {
	
	res := make([]string, len(names) + 1)
	
	res[0] = currentPath + "/" + name1
	for i, na := range names {
		res[i + 1] = currentPath + "/" + na
	}
	
	return res
}

func varsJsonHandlr(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Query().Get("req") {
		case "mesurment":
			err := json.NewEncoder(w).Encode(results)
			if err != nil {
				log.Print(err.Error())
				return
			}
			log.Print("Sending mesurment")
		default :
			fmt.Fprintf(w, "No requestParameters")
			log.Printf("Unknown api request: %s", r.Form)
	}
}

func indexHandlr(w http.ResponseWriter, r *http.Request) {
    t, err := template.ParseFiles(patchPath(
    	"templates/index.html", 
    	"templates/header.html", 
    	"templates/footer.html")...)
    
    if err != nil {
    	fmt.Fprintf(w, "Error %s", err.Error())
    	return
    }
    
    t.ExecuteTemplate(w, "index", nil)
}

func assertsServer(w http.ResponseWriter, r *http.Request) {
	fileserverHandler.ServeHTTP(w, r)
}